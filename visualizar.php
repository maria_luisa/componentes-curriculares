<!DOCTYPE html>
<html lang="en">
 
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="visualizar.css">
  <title>Visualizar Galeria</title>
</head>
 
<body>
  <h1>GALERIA</h1>
 
  <div class="row">
  <?php
 
  $array_imagens = glob("fotos/*.*"); 
 
  for ($i = 0; $i < count($array_imagens); $i++) {
    echo "<div class='coluna'>";
    if (!empty($array_imagens[$i])) {
      echo '<img src="' . $array_imagens[$i] . '">';
    }
    echo "</div>";
  }
 
  ?>
 
  </div>
</body>
 
</html>