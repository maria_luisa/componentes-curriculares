<?php
if (!file_exists("fotos")) {
    mkdir("fotos");
}
 
$dir = "fotos/";
 
$array_imagens = array();
 
$files = $_FILES["arquivos"];
 
if (!is_uploaded_file($_FILES['arquivos']['tmp_name'])) {
    header("location: index.html");
}
 
$extension = pathinfo($_FILES['arquivos']['name'], PATHINFO_EXTENSION);
 
if ($extension == "png") {
    move_uploaded_file($_FILES['arquivos']['tmp_name'], "$dir/" . uniqid() . "." . $extension);
    echo "<script> window.alert('Upload realizado com sucesso!')
    window.location.href='index.html'; </script>";  
} else {
    echo "<script> window.alert('Foi mal! :( O arquivo precisa ser do tipo .PNG.') 
    window.location.href='index.html'; </script>"; 
}
?>
